﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Estadistica.Startup))]
namespace Estadistica
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
